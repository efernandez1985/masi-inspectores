import { Injectable } from "@angular/core";
import { AppSettings } from "../app/app.settings";
import { HttpClient } from "../app/app.http";

@Injectable()
export class CompaniesService {

  constructor(protected http: HttpClient) {}

  public index() {
    return this.http.get(AppSettings.API_URL + 'companies');
  }

  public show(routeID) {
    return this.http.get(AppSettings.API_URL + 'companies/' + routeID);
  }

  public create(route) {
    return this.http.post(AppSettings.API_URL + 'companies', route);
  }

  public update(routeID, route) {
    return this.http.patch(AppSettings.API_URL + 'companies/' + routeID, route);
  }
}
