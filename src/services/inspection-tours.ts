import { Injectable } from "@angular/core";
import { AppSettings } from "../app/app.settings";
import { HttpClient } from "../app/app.http";

@Injectable()
export class InspectionToursService {

  constructor(protected http: HttpClient) {}

  public index() {
    return this.http.get(AppSettings.API_URL + 'inspection_tours');
  }

  public show(inspectionTourId) {
    return this.http.get(AppSettings.API_URL + 'inspection_tours/' + inspectionTourId);
  }

  public create(inspectionTour) {
    return this.http.post(AppSettings.API_URL + 'inspection_tours', inspectionTour);
  }

  public update(inspectionTourId, inspectionTour) {
    return this.http.patch(AppSettings.API_URL + 'inspection_tours/' + inspectionTourId, inspectionTour);
  }
}
