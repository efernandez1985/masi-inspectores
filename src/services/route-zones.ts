import { Injectable } from "@angular/core";
import { AppSettings } from "../app/app.settings";
import { HttpClient } from "../app/app.http";

@Injectable()
export class RouteZonesService {

  constructor(protected http: HttpClient) {}

  public index() {
    return this.http.get(AppSettings.API_URL + 'route_zones');
  }

  public show(routeID) {
    return this.http.get(AppSettings.API_URL + 'route_zones/' + routeID);
  }

  public showjointours(routeID,inspectionId) {
    return this.http.get(AppSettings.API_URL + 'route_zones/showjointours/' + routeID+'/'+inspectionId);
  }
  public create(route) {
    return this.http.post(AppSettings.API_URL + 'route_zones', route);
  }

  public update(routeID, route) {
    return this.http.patch(AppSettings.API_URL + 'route_zones/' + routeID, route);
  }
}
