import { Http, Response, Headers } from '@angular/http';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { AppSettings } from "../app/app.settings";

@Injectable()
export class AuthService {
  constructor( public http: Http) {
  }

  authenticate(loginauthData): Promise<Response> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(AppSettings.API_URL + 'auth/sign_in', loginauthData, { headers: headers })
      .catch((err) => {
        return Observable.of(err);
      })
      .toPromise();
  }

  logout(){
    return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append('X-Auth-Token', localStorage.getItem('token'));

        this.http.post(AppSettings.API_URL + 'logout', {}, {headers: headers})
          .subscribe(res => {
            localStorage.clear();
          }, (err) => {
            reject(err);
          });
    });
  }

}
