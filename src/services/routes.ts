import { Injectable } from "@angular/core";
import { AppSettings } from "../app/app.settings";
import { HttpClient } from "../app/app.http";

@Injectable()
export class RoutesService {

  constructor(protected http: HttpClient) {}

  public index() {
    return this.http.get(AppSettings.API_URL + 'routes');
  }

  public show(routeID) {
    return this.http.get(AppSettings.API_URL + 'routes/' + routeID);
  }

  public create(route) {
    return this.http.post(AppSettings.API_URL + 'routes', route);
  }

  public update(routeID, route) {
    return this.http.patch(AppSettings.API_URL + 'routes/' + routeID, route);
  }

  public getbycompany(id){
    return this.http.get(AppSettings.API_URL + 'routes/' + id+'/getbycompany');
  }
}
