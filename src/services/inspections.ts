import { Injectable } from "@angular/core";
import { AppSettings } from "../app/app.settings";
import { HttpClient } from "../app/app.http";

@Injectable()
export class InspectionsService {

  constructor(protected http: HttpClient) {}

  public index() {
    return this.http.get(AppSettings.API_URL + 'inspections');
  }

  public show(inspectionId) {
    return this.http.get(AppSettings.API_URL + 'inspections/' + inspectionId);
  }

  public create(inspection) {
    return this.http.post(AppSettings.API_URL + 'inspections', inspection);
  }

  public update(inspectionId, inspection) {
    return this.http.patch(AppSettings.API_URL + 'inspections/' + inspectionId, inspection);
  }

  public getbyuser(userId) {
    return this.http.get(AppSettings.API_URL + 'inspections/' + userId+'/getbyuser');
  }

  public getlastinspection(routeid, userId) {
    return this.http.get(AppSettings.API_URL + 'inspections/' + routeid+'/' + userId+'/getlastinspection');
  }

}
