import { Injectable } from "@angular/core";
import { AppSettings } from "../app/app.settings";
import { HttpClient } from "../app/app.http";

@Injectable()
export class InspectionPointsService {

  constructor(protected http: HttpClient) {}

  public index() {
    return this.http.get(AppSettings.API_URL + 'inspection_points');
  }

  public show(inspectionPointId) {
    return this.http.get(AppSettings.API_URL + 'inspection_points/' + inspectionPointId);
  }
  public showjointours(inspectionPointId,inspectionId) {
    return this.http.get(AppSettings.API_URL + 'inspection_points/showjointours/' + inspectionPointId+'/'+inspectionId);
  }

  public create(inspectionPoint) {
    return this.http.post(AppSettings.API_URL + 'inspection_points', inspectionPoint);
  }

  public update(inspectionPointId, inspectionPoint) {
    return this.http.patch(AppSettings.API_URL + 'inspection_points/' + inspectionPointId, inspectionPoint);
  }
  public getbybarcode(barcode) {
    return this.http.get(AppSettings.API_URL + 'inspection_points/' + barcode+'/getbybarcode');
  }
}
