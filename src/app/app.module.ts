// Main modules
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, NO_ERRORS_SCHEMA  } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import {ProgressBarModule} from "angular-progress-bar"

// Default pages
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

// Modules
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from "@ionic-native/keyboard";
import { HeaderColor } from "@ionic-native/header-color";
import { HttpModule } from '@angular/http';
import { HttpClient } from "./app.http";
import { IonicStorageModule } from "@ionic/storage";
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Camera } from '@ionic-native/camera';
import { ZBar } from '@ionic-native/zbar';

import { Geolocation  } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { File } from '@ionic-native/file';
// Services
import { AuthService } from '../services/auth';
import { InspectionsService } from "../services/inspections";
import { InspectionPointsService } from "../services/inspection-points";
import { InspectionToursService } from "../services/inspection-tours";
import { RoutesService } from "../services/routes";
import { RouteZonesService } from "../services/route-zones";
import { CompaniesService } from "../services/companies";

// Custom Pages
import { LoginPage } from '../pages/login/login';
import { InspectionPage } from '../pages/inspections/show';
import { AddInspectionPointPage } from '../pages/add-inspection-point/add-inspection-point';
import { InspectionToursPage } from '../pages/inspection-tours/inspection-tours';
import { EditInspectionTourPage } from '../pages/edit-inspection-tour/edit-inspection-tour';
import { AddRouteZonePage } from '../pages/add-route-zone/add-route-zone';
import { LocationsPage } from '../pages/locations/locations';
import { ReportsPage } from '../pages/reports/reports';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
  LoginPage,
    InspectionPage,
     // AddInspectionPointPage,
     // InspectionToursPage,
     // EditInspectionTourPage,
     // AddRouteZonePage,
     // LocationsPage,
     // ReportsPage
     

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{
      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
      
    }),
    IonicStorageModule.forRoot(),
    HttpModule,
    ProgressBarModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    InspectionPage,
    AddInspectionPointPage,
    InspectionToursPage,
    EditInspectionTourPage,
    AddRouteZonePage,
    LocationsPage,
    ReportsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Keyboard,
    HeaderColor,
    HttpClient,
    AuthService,
    InspectionsService,
    InspectionPointsService,
    RoutesService,
    RouteZonesService,
    CompaniesService,
    InspectionToursService,
    AndroidPermissions,
    Camera,
    ZBar,
    Geolocation,
    LocationAccuracy,
    File
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class AppModule {}
