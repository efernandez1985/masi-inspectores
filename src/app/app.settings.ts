export class AppSettings {
  // Current Version
  public static APP_VERSION = 'v0.0.5';

  // Production Settings
  public static API_URL = 'http://54.215.156.89/'
 
  //public static API_URL = 'http://192.168.8.4:3000/'

  //public static APP_WEBSITE = 'http://www.masi.mx/'
  public static APP_WEBSITE = 'http://masi.htdevs.com/'
  //public static APP_WEBSITE = 'http://b0cabd81.ngrok.io/'

  // Slack Integration
  // public static SLACK_API = 'https://hooks.slack.com/services/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';
  // public static SLACK_API = 'https://hooks.slack.com/services/T6M0VML2V/';
}
