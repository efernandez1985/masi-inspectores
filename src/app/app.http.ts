import { Headers, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from "./app.settings";

@Injectable()
export class HttpClient {

  public access_token: string = null;
  public client: string = null;
  public uid: string = null;

  constructor(public http: Http) {}

  public createAuthorizationHeader(): Headers {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('access-token', '' + this.access_token);
    headers.append('uid', '' + this.uid);
    headers.append('client', '' + this.client);
    return headers;
  }

  public get(url) {
    let headers = this.createAuthorizationHeader();

    return this.http.get(url, {
      headers
    }).catch((err) => {
      if (err.status === 401) {
        localStorage.clear();
        return Observable.throw(err);
      } else {
        return Observable.throw(err);
      }
    });
  }

  public delete(url) {
    let headers = this.createAuthorizationHeader();
    return this.http.delete(url, {
      headers
    }).catch((err) => {
      if (err.status === 401) {
        localStorage.clear();
        return Observable.throw(err);
      } else {
        return Observable.throw(err);
      }
    });
  }

  public post(url, data) {
    let headers = this.createAuthorizationHeader();
    return this.http.post(url, data, {
      headers
    }).catch((err) => {
      if (err.status === 401) {
        localStorage.clear();
        return Observable.throw(err);
      } else {
        return Observable.throw(err);
      }
    });
  }

  // public postUpload(url, data) {
  //   let headers = this.createAuthorizationHeaderUpload();
  //   return this.http.post(url, data, {
  //     headers
  //   }).catch((err) => {
  //     if (err.status === 401) {
  //       localStorage.clear();
  //       return Observable.throw(err);
  //     } else {
  //       return Observable.throw(err);
  //     }
  //   });
  // }

  public patch(url, data) {
    let headers = this.createAuthorizationHeader();
    return this.http.patch(url, data, {
      headers
    }).catch((err) => {
      if (err.status === 401) {
        localStorage.clear();
        return Observable.throw(err);
      } else {
        return Observable.throw(err);
      }
    });
  }

  public put(url, data) {
    let headers = this.createAuthorizationHeader();
    return this.http.put(url, data, {
      headers
    }).catch((err) => {
      if (err.status === 401) {
        localStorage.clear();
        return Observable.throw(err);
      } else {
        return Observable.throw(err);
      }
    });
  }
}
