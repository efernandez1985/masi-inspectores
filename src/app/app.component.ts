import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';

import { LoadingController } from "ionic-angular";
import { Storage } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from "@ionic-native/keyboard";
import { HeaderColor } from "@ionic-native/header-color";
import { HttpClient } from "./app.http";

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { LocationsPage } from '../pages/locations/locations';
import { ReportsPage } from '../pages/reports/reports';


@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;
  authData: any = "";
  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public keyboard: Keyboard, private headerColor: HeaderColor,
            public storage: Storage, protected loadingCtrl: LoadingController, public http: HttpClient) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Inicio', component: HomePage },
      { title: 'Ubicaciones', component: LocationsPage },
      { title: 'Reportes', component: ReportsPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Init the App
      this.statusBar.styleDefault();
      this.keyboard.disableScroll(true);
      this.rootPage = LoginPage;

      // Start Loader
      this.headerColor.tint('#9a1418');
      this.statusBar.backgroundColorByHexString('#9a1418');
      if(this.platform.is('ios')){
        this.statusBar.overlaysWebView(false);
      }

      this.storage.get('authData')
        .then((data) => {
          let authData = JSON.parse(data);
          if (authData != null && authData.access_token != undefined) {
            // Start the login message
            let loader = this.loadingCtrl.create({
              content: "Iniciando sesión...",
            });
            loader.present();

            // Send it to http client.
            this.http.access_token = authData.access_token;
            this.http.uid = authData.uid;
            this.http.client = authData.client;

            // Set the homepage
            this.rootPage = HomePage;
            loader.dismiss();
          }
        })
    });
    this.splashScreen.hide();
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout() {
    this.storage.clear();
    this.nav.setRoot(LoginPage);
  }
}
