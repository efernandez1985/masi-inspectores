import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NavController, App, LoadingController, ToastController, NavParams } from 'ionic-angular';

import { InspectionPage } from "../inspections/show";
import { RouteZonesService } from "../../services/route-zones";
/**
 * Generated class for the AddRouteZonePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-route-zone',
  templateUrl: 'add-route-zone.html',
})
export class AddRouteZonePage {
	routeId: string;
	companyName: string;
	routeName: string;
	inspectionId: string;
	constructor(public navCtrl: NavController, public navParams: NavParams,
		protected routeZonesService: RouteZonesService,
        public loadingCtrl: LoadingController,
        private toastCtrl: ToastController) {
	}

  	ngOnInit(): void {
		this.routeId = this.navParams.get('routeId');
	    this.companyName = this.navParams.get('companyName');
	    this.routeName = this.navParams.get('routeName');
    	this.inspectionId = this.navParams.get('inspectionId');
	}

	saveRouteZone(form: NgForm){
      let routeZoneData = {
        "route_id": this.routeId,
        "name": form.value.name,
      };
      let loader = this.loadingCtrl.create({
          content: "Guardando...",
          duration: 10000
      });
      loader.present();
      this.routeZonesService.create(routeZoneData)
      .subscribe(
        (response) =>{
          console.log(response);
          loader.dismiss();
          this.presentToast("Guardado correctamente");
          
        }


      );
  }
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      this.navCtrl.push(InspectionPage, {inspectionId: this.inspectionId})
    });

    toast.present();
  }
}
