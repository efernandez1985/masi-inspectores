import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddRouteZonePage } from './add-route-zone';

@NgModule({
  declarations: [
    AddRouteZonePage,
  ],
  imports: [
    IonicPageModule.forChild(AddRouteZonePage),
  ],
})
export class AddRouteZonePageModule {}
