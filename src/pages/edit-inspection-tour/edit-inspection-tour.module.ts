import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditInspectionTourPage } from './edit-inspection-tour';

@NgModule({
  declarations: [
    EditInspectionTourPage,
  ],
  imports: [
    IonicPageModule.forChild(EditInspectionTourPage),
  ],
})
export class EditInspectionTourPageModule {}
