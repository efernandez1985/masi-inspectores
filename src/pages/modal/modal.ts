import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {
	barcode: string = "";
  constructor(public navCtrl: NavController, public navParams: NavParams, private view: ViewController) {
  }

  closeModal(){

  	this.view.dismiss();
  }
  sendBarcode(){
  	console.log(this.barcode);
  	this.view.dismiss(this.barcode);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
  }


}
