import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InspectionToursPage } from './inspection-tours';

@NgModule({
  declarations: [
    InspectionToursPage,
  ],
  imports: [
    IonicPageModule.forChild(InspectionToursPage),
  ],
})
export class InspectionToursPageModule {}
