import { Component, ViewChild, ElementRef } from '@angular/core';
import { NgForm, FormGroup } from '@angular/forms';
import { NavController, App, LoadingController, ToastController, NavParams, Platform  } from 'ionic-angular';
import { AuthService } from '../../services/auth';
import { InspectionsService } from "../../services/inspections";
import { InspectionPointsService } from "../../services/inspection-points";
import { InspectionToursService } from "../../services/inspection-tours";
import { RoutesService } from "../../services/routes";
import { InspectionPage } from "../inspections/show";

import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Camera, CameraOptions } from '@ionic-native/camera';
/**

/**
 * Generated class for the InspectionToursPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
// export class Item {

//   constructor(fields: any) {
//     // Quick and dirty extend/assign fields to this model
//     for (const f in fields) {
//       // @ts-ignore
//       this[f] = fields[f];
//     }
//   }

// }

// export interface Questions {
//   [prop: string]: any;
// }

@IonicPage()
@Component({
  selector: 'page-inspection-tours',
  templateUrl: 'inspection-tours.html',
})


export class InspectionToursPage {


	inspectionId: string;
	item_id: string;
	companyName: string;
	routeName: string;
	pointId: string;
	pointName: string;
	inspPointName: string;
	pointType: string;
	pointSubType: string;
	pointSubType2: string;
	pointSubType3: string;
	pointIdTour: string;
	dateMinLimit = '2008-12-31';
	dateMaxLimit = '2028-12-31';
	hidrostatica = '';
	// questions: Questions[] = [];
	public photos : any;
	public photosAccess : any;
  	public base64Image : string;
  	//@ViewChild('f') form: any;
  	loader: any;
  	form: string[];
  	reposicion = '';
  	extintor_fechavencimiento = '';
  	extintor_fechacarga = '';
  	extintor_antiguedad_ilegible = false;
  constructor(private app: App,
  		private navParams: NavParams,
        public navCtrl: NavController,
        public authService: AuthService,
        public loadingCtrl: LoadingController,
        private toastCtrl: ToastController,
        protected inspectionsService: InspectionsService,
        protected inspectionPointsService: InspectionPointsService,
        protected inspectionToursService: InspectionToursService,
        protected routesService: RoutesService,
        private androidPermissions: AndroidPermissions,  
        private camera: Camera,
        public storage: Storage,
        public platform : Platform ) {}


	
  	ngOnInit(): void {
  		this.photos = [];
  		this.inspectionId = this.navParams.get('inspectionId');
  		this.item_id = this.navParams.get('item_id');
  		this.companyName = this.navParams.get('companyName');
  		this.routeName = this.navParams.get('routeName');
  		this.pointId = this.navParams.get('pointId');
  		this.pointName = this.navParams.get('pointName');
  		this.inspPointName = this.navParams.get('inspPointName');
  		this.pointType = this.navParams.get('pointType');
  		this.pointSubType = this.navParams.get('pointSubType');
  		this.pointSubType2 = this.navParams.get('pointSubType2');
  		this.pointSubType3 = this.navParams.get('pointSubType3');
  		this.pointIdTour = this.navParams.get('pointIdTour');
  		this.hidrostatica = this.navParams.get('hidrostatica');

  		
  // 		let questions = [
		// 	{
		// 		"text": "¿funciono?",
		// 		"type": "text",
		// 		"answers": "nada",
		// 		"required": 'form.value.extintor_tipo',
		// 		"required_answer": "A"
		// 	}
			
		// ];

		// for (let question of questions) {
		// 	this.questions.push(new Item(question));
		// }


	
	    this.requestPermission();

	 //    this.storage.get('dataForm').then(data => {
		//    console.log(data);
		//    this.form = data;
		// });

	   

	}
	
	requestPermission(){
		this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
	        result => {
	          console.log('Has permission?',result);
	          if(!result.hasPermission){
	            this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA).then(
	              result => {
	                console.log('Has permission?', result);
	                if(!result.hasPermission){
	                  //this.navCtrl.popToRoot()
	                }
	            },
	              err => console.log('Error requestPermission: ', err)
	          );
	          }
	        },
	        err => console.log('Error checkPermission: ', err)
	    );
	}

	deletePhoto(index){
	   this.photos.splice(index, 1);
	}

	takePhoto(e) {
		e.preventDefault();
	    const options : CameraOptions = {
	      quality: 50, // picture quality
	      destinationType: this.camera.DestinationType.DATA_URL,
	      encodingType: this.camera.EncodingType.JPEG,
	      mediaType: this.camera.MediaType.PICTURE,
	      targetHeight: 512,
	      targetWidth : 512
	    }
	    this.camera.getPicture(options) .then((imageData) => {
	        this.base64Image = "data:image/jpeg;base64," + imageData;
	        this.photos.push(this.base64Image);
	        this.photos.reverse();
	      }, (err) => {
	        console.log(err);
	    });
	    


	}
	public diff_months(dt2, dt1){

	  var diff =(dt2.getTime() - dt1.getTime()) / 1000;
	   diff /= (60 * 60 * 24 * 7 * 4);
	  return Math.round(diff);
	  
	}
	public checkDate(form: NgForm, status = 1){
		let loader = this.loadingCtrl.create({
	          content: "Validando datos...",
	          duration: 10000
	    });

	    loader.present();
		//let parts = form.value.extintor_fechacarga.split("-");
		//let extintor_fechacarga = parts[2] + "-" + parts[0] + "-" + parts[1];
		if (this.pointType == 'extintor') {
			let cargaDate =  new Date(form.value.extintor_fechacarga+"-01");
			let vencimientoDate =  new Date(form.value.extintor_fechavencimiento+"-01");
			let today = new Date();

			let cargaAge = this.diff_months(today, cargaDate);
			let vencimientoAge = this.diff_months(vencimientoDate, today);
			let diffCargaVencimiento = this.diff_months(vencimientoDate, cargaDate);
			console.log(cargaAge);
			let maxAge = 13;

			if (this.pointSubType2 == 'HALOTRON') {
				maxAge = 65
			}
			console.log(cargaAge);
			console.log(vencimientoAge);
			console.log(maxAge);

			if (form.value.extintor_fechacarga == '' || form.value.extintor_fechavencimiento == '') {
				loader.dismiss();
				let toast = this.presentToast("Fecha de carga o vencimiento vencida");
				toast.onDidDismiss(() => {
					return;
				});
			}else{
				if (diffCargaVencimiento !== maxAge ) {
					loader.dismiss();
					let toast = this.presentToast("Fecha de carga o vencimiento vencida");
					toast.onDidDismiss(() => {
						return;
					});
				}else{
					loader.dismiss();
					this.saveInspectionTour(form, status);
				}
			}
			
		}else{
			loader.dismiss();
			this.saveInspectionTour(form, status);
		}

	}
	public saveInspectionTour(form: NgForm, status = 1){
		let loader = this.loadingCtrl.create({
	          content: "Guardando...",
	          duration: 10000
	    });

	    loader.present();
		let inspectionTourData = {
			"inspection_id": this.inspectionId,
			"inspection_point_id": this.pointId,
			"item_id" : this.item_id,
			"status": status,
			//"extintor_tipo": form.value.extintor_tipo.toUpperCase(),
			"extintor_accesible": form.value.extintor_accesible.toUpperCase(),
			"extintor_senalizado": form.value.extintor_senalizado.toUpperCase(),
			"extintor_delimitado": form.value.extintor_delimitado.toUpperCase(),
			"extintor_gabinete": JSON.stringify(form.value.extintor_gabinete).toUpperCase(),
			"extintor_antiguedad": (this.extintor_antiguedad_ilegible) ? 'ILEGIBLE' : form.value.extintor_antiguedad.toUpperCase(),
			"extintor_carga": form.value.extintor_carga.toUpperCase(),
			"extintor_manguera": form.value.extintor_manguera.toUpperCase(),
			"extintor_cilindro": form.value.extintor_cilindro.toUpperCase(),
			"extintor_fechacarga": form.value.extintor_fechacarga.toUpperCase(),
			"extintor_fechavencimiento": form.value.extintor_fechavencimiento.toUpperCase(),
			//"extintor_fechaprueba": form.value.extintor_fechaprueba.toUpperCase(),
			"extintor_observaciones": form.value.extintor_observaciones.toUpperCase(),
			"extintor_etiquetaInspeccion": form.value.extintor_etiquetaInspeccion.toUpperCase(),
			"extintor_seguro": form.value.extintor_seguro.toUpperCase(),
			"extintor_hidrostatica": this.hidrostatica,
			"extintor_altura": form.value.extintor_altura.toUpperCase(),
			//"hidrante_tipo": form.value.hidrante_tipo.toUpperCase(),
			"hidrante_accesible": form.value.hidrante_accesible.toUpperCase(),
			"hidrante_senalizado": form.value.hidrante_senalizado.toUpperCase(),
			"hidrante_delimitado": form.value.hidrante_delimitado.toUpperCase(),
			"hidrante_gabinete": JSON.stringify(form.value.hidrante_gabinete).toUpperCase(),
			"hidrante_manguera": form.value.hidrante_manguera.toUpperCase(),
			//"hidrante_fechaprueba": form.value.hidrante_fechaprueba.toUpperCase(),
			"hidrante_chiflon": form.value.hidrante_chiflon.toUpperCase(),
			"hidrante_coples": form.value.hidrante_coples.toUpperCase(),
			"hidrante_espuma": form.value.hidrante_espuma.toUpperCase(),
			"hidrante_instrucciones": form.value.hidrante_instrucciones.toUpperCase(),
			"hidrante_valvula": form.value.hidrante_valvula.toUpperCase(),
			"hidrante_observaciones": form.value.hidrante_observaciones.toUpperCase(),
			//"regadera_tipo": form.value.regadera_tipo.toUpperCase(),
			"regadera_accesible": form.value.regadera_accesible.toUpperCase(),
			"regadera_senalizado": form.value.regadera_senalizado.toUpperCase(),
			"regadera_delimitado": form.value.regadera_delimitado.toUpperCase(),
			"regadera_suficiente": form.value.regadera_suficiente.toUpperCase(),
			"regadera_velocidad": form.value.regadera_velocidad.toUpperCase(),
			"regadera_contaminacion": form.value.regadera_contaminacion.toUpperCase(),
			"regadera_altura": form.value.regadera_altura.toUpperCase(),
			"regadera_posicion": form.value.regadera_posicion.toUpperCase(),
			"regadera_difusorde": form.value.regadera_difusorde.toUpperCase(),
			"regadera_estadoRociadores": form.value.regadera_estadoRociadores.toUpperCase(),
			"regadera_taponRociadores": form.value.regadera_taponRociadores.toUpperCase(),
			"regadera_portatil": form.value.regadera_portatil.toUpperCase(),
			//"regadera_distancia": form.value.regadera_distancia.toUpperCase(),
			"regadera_observaciones": form.value.regadera_observaciones.toUpperCase(),
			"regadera_etiquetaInspeccion": form.value.regadera_etiquetaInspeccion.toUpperCase(),
			//"camilla_tipo": form.value.camilla_tipo.toUpperCase(),
			"camilla_accesible": form.value.camilla_accesible.toUpperCase(),
			"camilla_senalizado": form.value.camilla_senalizado.toUpperCase(),
			"camilla_delimitado": form.value.camilla_delimitado.toUpperCase(),
			"camilla_gabinete": JSON.stringify(form.value.camilla_gabinete).toUpperCase(),
			"camilla_sujeccion": form.value.camilla_sujeccion.toUpperCase(),
			"camilla_estadoCamilla": form.value.camilla_estadoCamilla.toUpperCase(),
			"camilla_inmovilizacion": form.value.camilla_inmovilizacion.toUpperCase(),
			"camilla_observaciones": form.value.camilla_observaciones.toUpperCase(),
			"camilla_etiquetaInspeccion": form.value.camilla_etiquetaInspeccion.toUpperCase(),
			//"bombero_tipo": form.value.bombero_tipo.toUpperCase(),
			"bombero_accesible": form.value.bombero_accesible.toUpperCase(),
			"bombero_senalizado": form.value.bombero_senalizado.toUpperCase(),
			"bombero_delimitado": form.value.bombero_delimitado.toUpperCase(),
			"bombero_gabinete": JSON.stringify(form.value.bombero_gabinete).toUpperCase(),
			"bombero_casco": form.value.bombero_casco.toUpperCase(),
			"bombero_pantalon": form.value.bombero_pantalon.toUpperCase(),
			"bombero_chaqueton": form.value.bombero_chaqueton.toUpperCase(),
			"bombero_bota": form.value.bombero_bota.toUpperCase(),
			"bombero_guantes": form.value.bombero_guantes.toUpperCase(),
			"bombero_SCBA": form.value.bombero_SCBA.toUpperCase(),
			"bombero_estadoEquipo": form.value.bombero_estadoEquipo.toUpperCase(),
			"bombero_cargadoSCBA": form.value.bombero_cargadoSCBA.toUpperCase(),
			"bombero_observaciones": form.value.bombero_observaciones.toUpperCase(),
			"bombero_etiquetaInspeccion": form.value.bombero_etiquetaInspeccion.toUpperCase(),
			"bombero_monja": form.value.bombero_monja.toUpperCase(),
			"camilla_funda": form.value.camilla_funda.toUpperCase(),
			"camilla_ara": form.value.camilla_ara.toUpperCase(),
			"camilla_pulpo": form.value.camilla_pulpo.toUpperCase(),
			"extintor_reposicion": form.value.extintor_reposicion,
			"camilla_reposicion": form.value.camilla_reposicion,
			"photo1": (this.photos[0]) ? this.photos[0] : "",
			"photo2": (this.photos[1]) ? this.photos[1] : "",
			"photo3": (this.photos[2]) ? this.photos[2] : "",
			"photo4": (this.photos[3]) ? this.photos[3] : "",
			"photo5": (this.photos[4]) ? this.photos[4] : "",
			"photoAccess": (this.photosAccess) ? this.photosAccess : ""


		};

	    
	    this.inspectionToursService.create(inspectionTourData)
	    .subscribe(
	        (response) =>{
	        	let it = JSON.parse(response.text());
				console.log(it);
				loader.dismiss();
				let toast = this.presentToast("Guardado correctamente");
				toast.onDidDismiss(() => {
					//this.navCtrl.setRoot(HomePage);
					if(status == 0){
						//this.platform.exitApp();
						this.navCtrl.setRoot(HomePage, {saveExit: true});
					}else{
						this.navCtrl.push(InspectionPage, {inspectionId: this.inspectionId})
					}
				});
	        }


	   	);

	}

	savePreview(e, form: NgForm){

		e.preventDefault();
		this.checkDate(form, 0);



	}
	changeReposicion(e){
		console.log(e);
		if (e.value) {
			this.reposicion = 'REPOSICIÓN';
		}else{
			this.reposicion = '';
		}


	}
	
	presentToast(msg) {
	    let toast = this.toastCtrl.create({
	      message: msg,
	      duration: 3000,
	      position: 'bottom',
	      dismissOnPageChange: true
	    });

	    

	    toast.present();
	    return toast;
	}

	takePhotoAccess(e, form: NgForm) {
		e.preventDefault();
	    const options : CameraOptions = {
	      quality: 50, // picture quality
	      destinationType: this.camera.DestinationType.DATA_URL,
	      encodingType: this.camera.EncodingType.JPEG,
	      mediaType: this.camera.MediaType.PICTURE,
	      targetHeight: 512,
	      targetWidth : 512
	    }
	    this.camera.getPicture(options) .then((imageData) => {
	        this.base64Image = "data:image/jpeg;base64," + imageData;
	        this.photosAccess = this.base64Image;
	       	
			this.saveInspectionTour(form, 2);
	      }, (err) => {
	        console.log(err);
	    });
	    
	}

	changeFechaCarga(e){
		console.log(e);

		// if (e.value) {
		// 	this.reposicion = 'REPOSICIÓN';
		// }else{
		// 	this.reposicion = '';
		// }
		if (this.pointSubType2 == 'HALOTRON') {
			
			this.extintor_fechavencimiento = e.year+5+"-"+e.month;
		}else{

			this.extintor_fechavencimiento = e.year+1+"-"+e.month;
		}

	}

}
