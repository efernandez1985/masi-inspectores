import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController, MenuController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth';
import { AppSettings } from "../../app/app.settings";
import { Storage } from '@ionic/storage';
import { HttpClient } from "../../app/app.http";

import { HomePage } from '../home/home';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  appVersion: any;
  loading: any;
  data: any;

  constructor(public navCtrl: NavController, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController,
              public storage: Storage, public http: HttpClient, public menuCtrl: MenuController) {
                this.appVersion = AppSettings.APP_VERSION;
                // this.menuCtrl.enable(false, 'MainSideMenu');
              }

  protected loginData = {
    username: null,
    password: null
  };

  ngOnInit(): void {




  }
  onLogin(form: NgForm) {

    let loader = this.loadingCtrl.create({
      content: "Cargando...",
      duration: 10000
    });
    loader.present();

    let loginauthData = {"email": form.value.email, "password": form.value.password};
    this.authService.authenticate(loginauthData)
    .then(
      (response) => {
        if(response.status == 401) {
          this.presentToast("Email o contraseña incorrectos, favor de intentar de nuevo.");
        } else if(response.status == 200) {
          let userInfo = JSON.parse(response.text());
          userInfo = userInfo['data'];

          // Send it to http client.
          this.http.access_token = response.headers.get('access-token');
          this.http.uid = response.headers.get('uid');
          this.http.client = response.headers.get('client');

          // Save it for future use.
          this.storage.set('authData', JSON.stringify(
            {
              access_token: response.headers.get('access-token'),
              client: response.headers.get('client'),
              uid: response.headers.get('uid'),
              first_name: userInfo.first_name,
              last_name: userInfo.last_name,
              role_id: userInfo.role_id,
              userId: userInfo.id
            }));

          this.navCtrl.setRoot(HomePage, {saveExit: false});
        } else {
          this.presentToast("Algo salio mal, favor de intentar de nuevo.");
        }
        loader.dismiss();
      }
    );
  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Authenticating...'
    });

    this.loading.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      // console.log('Dismissed toast');
    });

    toast.present();
  }

}
