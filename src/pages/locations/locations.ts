import { Component } from '@angular/core';
import { NavController, App, LoadingController, NavParams, Platform,Modal, ModalController, ToastController  } from 'ionic-angular';
import { AuthService } from '../../services/auth';
import { CompaniesService } from "../../services/companies";
import { RoutesService } from "../../services/routes";
import { InspectionPointsService } from "../../services/inspection-points";

import { ZBar, ZBarOptions } from '@ionic-native/zbar';
/**
 * Generated class for the LocationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-locations',
  templateUrl: 'locations.html',
})
export class LocationsPage {
	inspectionPoints = [];
	companyName = "";
	routeName = "";
  companies = [];
  routes = [];
  protected inspectionPointsBarcode = {};
  barcode ='';
	constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public companiesService: CompaniesService,
    public routesService: RoutesService,
    public inspectionPointsService: InspectionPointsService,
      public modalCtrl: ModalController,
      private toastCtrl: ToastController,
      private zbar: ZBar

    ) {
	}

	ngOnInit(): void {


      // this.inspectionPoints = this.navParams.get('inspectionPoints');
      // this.companyName = this.navParams.get('companyName');
      // this.routeName = this.navParams.get('routeName');
      this.getCompanies();
  }
  getCompanies(){
    let loader = this.loadingCtrl.create({
      content: "Cargando datos...",
      duration: 10000
    });
    loader.present();

    this.companiesService.index()
      .subscribe(
        (response) => {
          let c = JSON.parse(response.text());
          console.log(c);
          this.companies = c;
          //this.companyName = insp.company.name;
          //this.routeName = insp.route.name;
          loader.dismiss();
          this.routes = [];
          this.inspectionPoints = [];
        }
      );


  }
  getRoute(idCompany){
    console.log('idCompany = '+ idCompany);
    this.routesService.getbycompany(idCompany)
      .subscribe(
        (response) => {
          let r = JSON.parse(response.text());
          console.log(r);
          this.routes = [];
          this.inspectionPoints = [];
          r.forEach( val=> {
            let route = val.route
            let rz = val.route.route_zone
            let ip = val.route.route_zone.inspection_points
            this.routes.push(route);
            //this.route_zone
          });

          //this.routes = r;
          //this.companyName = insp.company.name;
          //this.routeName = insp.route.name;
          //loader.dismiss();
        }
      );
  }

  getInspectionPoints(index){

    console.log('Route = '+ this.routes[index]);
    this.inspectionPoints = [];
    this.routes[index].route_zone.forEach( val=> {
      
      val.inspection_points.forEach( val2=> {
        val2['route_zone_name'] = val.name

        
        this.inspectionPoints.push(val2);
        
      });
      
      
    });
    
  }
  showModal(){

    let modal: Modal = this.modalCtrl.create("ModalPage");
    
    modal.onDidDismiss(data => {
      console.log(data);
      this.barcode = data;
      this.inspectionPoints=[];
      this.getInspectionPointsbybarcode(this.barcode);
    });

    modal.present();
  }

  getByBarcode(){
    console.log(this.inspectionPointsBarcode);
    this.zbar.scan({flash: 'on'}).then(barcodeData => {
      console.log('Barcode data', barcodeData);

      this.barcode = barcodeData;
      this.inspectionPoints=[];
      this.getInspectionPointsbybarcode(this.barcode);
      

    }).catch(err => {
      console.log('Error', err);
    });
  }

  getInspectionPointsbybarcode(barcode){
    this.inspectionPointsService.getbybarcode(barcode)
    .subscribe(
      (response) => {
        let r = JSON.parse(response.text());
        this.inspectionPoints = r;
        
      }
    );
  }
  presentToast(msg) {
      let toast = this.toastCtrl.create({
        message: msg,
        duration: 2000,
        position: 'bottom',
        dismissOnPageChange: true
      });

      

      toast.present();
      return toast;
  }


}
