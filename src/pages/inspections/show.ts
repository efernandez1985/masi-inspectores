import { Component, ViewChild } from '@angular/core';
import { NavController, App, LoadingController, ToastController, NavParams, AlertController,Modal, ModalController , ViewController, normalizeURL, Content, Platform } from 'ionic-angular';
import { AuthService } from '../../services/auth';
import { InspectionsService } from "../../services/inspections";
import { AddInspectionPointPage } from '../add-inspection-point/add-inspection-point';
import { AddRouteZonePage } from '../add-route-zone/add-route-zone';
import { InspectionToursPage } from '../inspection-tours/inspection-tours';
import { EditInspectionTourPage } from '../edit-inspection-tour/edit-inspection-tour';
import { HomePage } from '../home/home';
import { LocationsPage } from '../locations/locations';;

import { InspectionPointsService } from "../../services/inspection-points";
import { RoutesService } from "../../services/routes";
import { RouteZonesService } from "../../services/route-zones";
import { ZBar, ZBarOptions } from '@ionic-native/zbar';
import { Geolocation } from '@ionic-native/geolocation';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Storage } from '@ionic/storage';

import { AppSettings } from "../../app/app.settings";
import { File, IWriteOptions } from '@ionic-native/file';
@Component({
  selector: 'page-show',
  templateUrl: 'show.html'
})


export class InspectionPage {
  inspectionId: string;
  companyName: string;
  routeName: string;
  routeId: string;
  firstVisit = true;
  protected routeZones = [];
  protected inspection = [];
  protected inspections = [];
  protected inspectionPoints = [];
  protected inspectionPointsBarcode = {};
  inspectionStts = '';
  loader: any;
  barcode: string = "";
  item_barcode = "";
  item_id = "";
  totalPorcent = 0;
  startDate = '';
  name_signature = '';
  signatureDiv = false;

  @ViewChild('imageCanvas') canvas: any;
  canvasElement: any;
 
  saveX: number;
  saveY: number;
 
  storedImages = [];
 
  // Make Canvas sticky at the top stuff
  @ViewChild(Content) content: Content;
  @ViewChild('fixedContainer') fixedContainer: any;
 
  // Color Stuff
  selectedColor = '#000';
 
  
  constructor(private navParams: NavParams,
    public navCtrl: NavController,
      public authService: AuthService,
      public loadingCtrl: LoadingController,
      private toastCtrl: ToastController,
      protected inspectionsService: InspectionsService,
      protected inspectionPointService: InspectionPointsService,
      protected routesService: RoutesService,
      protected routeZonesService: RouteZonesService,
      private zbar: ZBar,
      private alertCtrl: AlertController,
      private geolocation: Geolocation,
      private androidPermissions: AndroidPermissions,
      private locationAccuracy: LocationAccuracy,
      public modalCtrl: ModalController,
      public storage: Storage,
      private plt: Platform,
      private file: File) {}

    ngOnInit(): void {

      
     
        this.getInspection();

        this.requestPermission();
    }
    /***********************/
    /***********************/
    /***********************/
    ionViewDidEnter() {
      // https://github.com/ionic-team/ionic/issues/9071#issuecomment-362920591
      // Get the height of the fixed item
      let itemHeight = this.fixedContainer.nativeElement.offsetHeight;
      let scroll = this.content.getScrollElement();
   
      // Add preexisting scroll margin to fixed container size
      itemHeight = Number.parseFloat(scroll.style.marginTop.replace("px", "")) + itemHeight;
      scroll.style.marginTop = itemHeight + 'px';
    }
   
    ionViewDidLoad() {
      // Set the Canvas Element and its size
      this.canvasElement = this.canvas.nativeElement;
      this.canvasElement.width = this.plt.width() + '';
      this.canvasElement.height = 200;
    }
    selectColor(color) {
      this.selectedColor = color;
    }
     
    startDrawing(ev) {
      ev.preventDefault();
      var canvasPosition = this.canvasElement.getBoundingClientRect();
     
      this.saveX = ev.touches[0].pageX - canvasPosition.x;
      this.saveY = ev.touches[0].pageY - canvasPosition.y;
    }
     
    moved(ev) {
      ev.preventDefault();
      var canvasPosition = this.canvasElement.getBoundingClientRect();
     
      let ctx = this.canvasElement.getContext('2d');
      let currentX = ev.touches[0].pageX - canvasPosition.x;
      let currentY = ev.touches[0].pageY - canvasPosition.y;
     
      ctx.lineJoin = 'round';
      ctx.strokeStyle = this.selectedColor;
      ctx.lineWidth = 5;
     
      ctx.beginPath();
      ctx.moveTo(this.saveX, this.saveY);
      ctx.lineTo(currentX, currentY);
      ctx.closePath();
     
      ctx.stroke();
     
      this.saveX = currentX;
      this.saveY = currentY;
    }
     
     
    saveCanvasImage() {
      var dataUrl = this.canvasElement.toDataURL();
     
      let ctx = this.canvasElement.getContext('2d');
      ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height); // Clears the canvas
     
      // let name = new Date().getTime() + '.png';
      // let path = this.file.dataDirectory;
      // let options: IWriteOptions = { replace: true };
     
      dataUrl
      //let blob = this.b64toBlob(data, 'image/png');
      //console.log(blob);
      // this.file.writeFile(path, name, blob, options).then(res => {
      //   this.storeImage(name);
      // }, err => {
      //   console.log('error: ', err);
      // });
      return dataUrl;
    }
    clearCanvas(){

      let ctx = this.canvasElement.getContext('2d');
      ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height); // Clears the canvas
    }
    // https://forum.ionicframework.com/t/save-base64-encoded-image-to-specific-filepath/96180/3
    b64toBlob(b64Data, contentType) {
      contentType = contentType || '';
      var sliceSize = 512;
      var byteCharacters = atob(b64Data);
      var byteArrays = [];
     
      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);
     
        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }
     
        var byteArray = new Uint8Array(byteNumbers);
     
        byteArrays.push(byteArray);
      }
     
      var blob = new Blob(byteArrays, { type: contentType });
      return blob;
    }

    storeImage(imageName) {
      // let saveObj = { img: imageName };
      // this.storedImages.push(saveObj);
      // this.storage.set(STORAGE_KEY, this.storedImages).then(() => {
      //   setTimeout(() =>  {
      //     this.content.scrollToBottom();
      //   }, 500);
      // });
    }
     
    removeImageAtIndex(index) {
      // let removed = this.storedImages.splice(index, 1);
      // this.file.removeFile(this.file.dataDirectory, removed[0].img).then(res => {
      // }, err => {
      //   console.log('remove err; ' ,err);
      // });
      // this.storage.set(STORAGE_KEY, this.storedImages);
    }
     
    getImagePath(imageName) {
      let path = this.file.dataDirectory + imageName;
      // https://ionicframework.com/docs/wkwebview/#my-local-resources-do-not-load
      path = normalizeURL(path);
      return path;
    }

    /***********************/
    /***********************/
    /***********************/
    requestPermission(){
      this.locationAccuracy.canRequest().then((canRequest: boolean) => {

        if(canRequest) {
          // the accuracy option will be ignored by iOS
          this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
            () => console.log('Request successful'),
            error => console.log('Error requesting location permissions', error)
          );
        }
      });
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(
            result => {
              console.log('Has permission?',result);
              if(!result.hasPermission){
                this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(
                  result => {
                    console.log('Has permission?', result);
                    if(!result.hasPermission){
                      //this.navCtrl.popToRoot()
                    }
                },
                  err => console.log('Error requestPermission: ', err)
              );
              }
            },
            err => console.log('Error checkPermission: ', err)
        );
    }
    protected getInspection() {
        this.inspection = [];
        this.inspectionId = this.navParams.get('inspectionId');
        this.loader = this.loadingCtrl.create({
          content: "Cargando Inspeccion...",
          duration: 10000
        });
        this.loader.present();

        this.inspectionsService.show(this.inspectionId)
          .subscribe(
            (response) => {
              //console.log(response);
              let insp = JSON.parse(response.text());
              console.log(insp);
              this.inspection = insp;
              this.inspectionStts = insp.status;
              this.companyName = insp.company.name;
              this.routeName = insp.route.name;
              this.routeId = insp.route.id;
              this.startDate = insp.started_at;
              //loader.dismiss();


              
              this.getPointTours();
            }
        );
    }
    getPointTours() {


        this.routeZonesService.showjointours(this.routeId,this.inspectionId)
          .subscribe(
            (response) => {
              //console.log(response);
              let ip = JSON.parse(response.text());
              //console.log(insp);
              //let ip = JSON.parse(responsePoint.text())['inspection_points'];
              console.log(ip);
              let introEdit = false;
              let totalPoint = 0;
              let pointTours = 0;

              ip.forEach( val=> {

                

                if (val.inspection_point_type != null) {

                  val.color =  '#fff';

                  

                  totalPoint++;

                  if (val.idTour !== null) {

                    switch(val.statusTour){
                      case 0: { 
                        //pendiente
                          val.color = '#fdd5ff';
                          break; 
                       } 
                       case 1: { 
                        //completado
                          val.color = '#b8ffb2';
                          break; 
                       } 
                       case 2: { 
                        //sin acceso
                          val.color = '#bcbaad';
                          break; 
                       } 
                       default: { 
                          val.color =  '#fff';
                          break; 
                       } 
                    }
                    if (val.statusTour != 0) {
                      pointTours++;
                    }
                    
                  }

                  switch(val.inspection_point_type){

                    case 'extintor': val.inspection_point_type_lbl = 'Extintor';
                      break;
                    case 'hidrante': val.inspection_point_type_lbl = 'Hidrante';
                      break;
                    case 'regadera': val.inspection_point_type_lbl = 'Regadera y/o lavaojos';
                      break;
                    case 'camilla': val.inspection_point_type_lbl = 'Camilla';
                      break;
                    case 'bombero': val.inspection_point_type_lbl = 'Eq. De Bomberos';
                      break;

                  }
                  // if (val.statusTour == 0 && !introEdit) {
                  //   introEdit = true;
                  //   this.showInspectionTour(this.inspectionId,this.companyName,this.routeName,val.idInsp,val.name,val.inspection_point_type,val.inspection_point_sub_type,val.inspection_point_sub_type2,val.inspection_point_sub_type3,val.idTour,val.inspPointName,val.hidrostatica, val.barcode,val.item_barcode)
                  //   //this.navCtrl.push(EditInspectionTourPage, {inspectionId:this.inspectionId,companyName:this.companyName,routeName:this.routeName,pointId:val.idInsp,pointName:val.name,pointType:val.inspection_point_type,pointSubType:val.inspection_point_sub_type,pointSubType2:val.inspection_point_sub_type2,pointSubType3:val.inspection_point_sub_type3,pointIdTour:val.idTour, inspPointName:val.inspPointName,hidrostatica:val.hidrostatica, val.barcode,val.item_barcode})
                  //   return;
                  // }

                  

                  this.inspectionPointsBarcode[val.barcode] = { 
                    "id": this.inspection,
                    "companyName": this.companyName,
                    "routeName":this.routeName,
                    "pointid":val.idInsp,
                    "pointIdTour":val.idTour,
                    "pointname":val.name,
                    "inspPointName":val.inspPointName,
                    "pointinspection_point_type":val.inspection_point_type,
                    "pointinspection_point_sub_type":val.inspection_point_sub_type,
                    "inspPointHidrostatica" : val.hidrostatica,
                    "location" : val.location,
                    "item_barcode" : val.item_barcode

                  }
                  this.inspectionPoints.push(val);
                }

              });
              this.totalPorcent = (100*pointTours)/totalPoint;
              console.log('totalPorcent = '+ this.totalPorcent);
              if (this.totalPorcent == 100) {
                //this.finishInspection();
              }
              
             this.getRouteZones();

             

            }
        );
    }
     //departamentos / route zone
    //sucursal / route
    getRouteZones(){

        this.routesService.show(this.routeId)
        .subscribe(
          (response) => {
            console.log(response);
            let rz = JSON.parse(response.text());
            this.routeZones = rz.route_zones;
            this.inspections = rz.inspections;
            //this.companyName = insp.company.name;
            //this.routeName = insp.route.name;
            //loader.dismiss();
            this.inspections.forEach(val=>{
              if (val.status == 'finalizada') {
                this.firstVisit = false;
              }

            })
             this.loader.dismiss();
          }
        );

    }
    getByBarcode(){
        console.log(this.inspectionPointsBarcode);
        this.zbar.scan({flash: 'on'}).then(barcodeData => {
          console.log('Barcode data', barcodeData);

          this.barcode = barcodeData;
          if (this.barcode in this.inspectionPointsBarcode) {

          
            let id = this.inspectionPointsBarcode[this.barcode].id.id;
            let companyName = this.inspectionPointsBarcode[this.barcode].companyName;
            let routeName = this.inspectionPointsBarcode[this.barcode].routeName;
            let pointid = this.inspectionPointsBarcode[this.barcode].pointid;
            let pointIdTour = this.inspectionPointsBarcode[this.barcode].pointIdTour;
            let pointname = this.inspectionPointsBarcode[this.barcode].pointname;
            let inspPointName = this.inspectionPointsBarcode[this.barcode].inspPointName;
            let pointinspection_point_type = this.inspectionPointsBarcode[this.barcode].pointinspection_point_type;
            let pointinspection_point_sub_type  = this.inspectionPointsBarcode[this.barcode].pointinspection_point_sub_type;
            let pointinspection_point_sub_type2  = this.inspectionPointsBarcode[this.barcode].pointinspection_point_sub_type2;
            let pointinspection_point_sub_type3  = this.inspectionPointsBarcode[this.barcode].pointinspection_point_sub_type3;
            let pointinspection_point_idTour  = this.inspectionPointsBarcode[this.barcode].pointinspection_point_sub_type;

            let inspPointHidrostatica = this.inspectionPointsBarcode[this.barcode].inspPointHidrostatica;
            this.item_barcode = this.inspectionPointsBarcode[this.barcode].item_barcode;


            if (pointinspection_point_type !== 'extintor' && pointinspection_point_type !== 'camilla') {
              if ( this.inspectionStts == 'agendada') {
                  this.startConfirm(id, companyName, routeName, pointid, pointname, pointinspection_point_type, pointinspection_point_sub_type, pointinspection_point_sub_type2, pointinspection_point_sub_type3, pointIdTour, inspPointName,inspPointHidrostatica, this.barcode,this.item_barcode, this.item_id);
                  
                }else{


                  if (pointIdTour == null) {
                    this.navCtrl.push(InspectionToursPage, {inspectionId:id,companyName:companyName,routeName:routeName,pointId:pointid,pointName:pointname,pointType:pointinspection_point_type,pointSubType:pointinspection_point_sub_type,pointSubType2:pointinspection_point_sub_type2,pointSubType3:pointinspection_point_sub_type3,pointIdTour:pointIdTour,inspPointName:inspPointName, hidrostatica:inspPointHidrostatica, item_id: this.item_id})

                  }else{
                    this.navCtrl.push(EditInspectionTourPage, {inspectionId:id,companyName:companyName,routeName:routeName,pointId:pointid,pointName:pointname,pointType:pointinspection_point_type,pointSubType:pointinspection_point_sub_type,pointSubType2:pointinspection_point_sub_type2,pointSubType3:pointinspection_point_sub_type3,pointIdTour:pointIdTour,inspPointName:inspPointName, hidrostatica:inspPointHidrostatica, item_id: this.item_id})

                  }
                }

            }else{
              this.showInspectionTour(id,companyName,routeName,pointid,pointname,pointinspection_point_type,pointinspection_point_sub_type,pointinspection_point_sub_type2,pointinspection_point_sub_type3,pointIdTour,inspPointName,inspPointHidrostatica, this.barcode,this.item_barcode, this.item_id);
            }
            
          }else{
            this.presentToast("Punto de inspección NO registrado");
          }

        }).catch(err => {
          console.log('Error', err);
        });
    }


    getInspectionPoints(arr_rz){
        arr_rz.forEach( valrz=> {

          this.routeZonesService.show(valrz.id)
          .subscribe(
            (responsePoint) => {
              let ip = JSON.parse(responsePoint.text())['inspection_points'];
              console.log(ip);
              ip.forEach( val=> {
                switch(val.inspection_point_type){

                  case 'extintor': val.inspection_point_type_lbl = 'Extintor';
                    break;
                  case 'hidrante': val.inspection_point_type_lbl = 'Hidrante';
                    break;
                  case 'regadera': val.inspection_point_type_lbl = 'Regadera y/o lavaojos';
                    break;
                  case 'camilla': val.inspection_point_type_lbl = 'Camilla';
                    break;
                  case 'bombero': val.inspection_point_type_lbl = 'Eq. De Bomberos';
                    break;

                }

                this.inspectionPointsBarcode[val.barcode] = { 
                  "id": this.inspection,
                  "companyName": this.companyName,
                  "routeName":this.routeName,
                  "pointid":val.idInsp,
                  "pointIdTour":val.idTour,
                  "pointname":val.name,
                  "pointinspection_point_type":val.inspection_point_type,
                  "pointinspection_point_sub_type":val.inspection_point_sub_type,
                  "inspPointHidrostatica" : val.hidrostatica,
                  "location" : val.location

                }
                this.inspectionPoints.push(val);
              });

              
              this.loader.dismiss();
            }
          )
        });

    }

    finishInspection(flag){
      this.signatureDiv = flag;
    }
    
    finishInspectionSignature(){

      this.loader = this.loadingCtrl.create({
        content: "Finalizando Inspeccion...",
        //duration: 10000
      });
      this.loader.present();

      let finishDate = this.calcTime("", -5);

      let s = new Date(this.startDate).getTime();
      let f = new Date(finishDate).getTime();
      if (f > s) {

      
        let lat: any = "";
        let lng: any = "";
        this.geolocation.getCurrentPosition().then((resp) => {

         let lat = resp.coords.latitude;
         let lng = resp.coords.longitude;
         let imgSignature = this.saveCanvasImage();
         this.inspectionsService.update(this.inspectionId,{'status':2, 'finished_at': finishDate, 'finished_lat':lat, 'finished_lng':lng, 'name_signature' : this.name_signature, 'signature': imgSignature})
          .subscribe(
            (response) => {

              this.loader.dismiss();
              let toast = this.presentToast("Inspección finalizada exitosamente");
              
              toast.onDidDismiss(() => {
                this.navCtrl.setRoot(HomePage);
              });
              
            }
        );
        }).catch((error) => {
          console.log('Error getting location', error);
        });
      }else{
        this.loader.dismiss();
        let toast = this.presentToast("ERROR - Fecha de inicio es igual o posterear a la fecha de finalización");
      }
       

        

    }
    calcTime(city, offset) {

        // create Date object for current location
        let d = new Date();
        
        // convert to msec
        // add local time zone offset 
        // get UTC time in msec
        let utc = d.getTime() + (d.getTimezoneOffset() * 60000);
        
        // create new Date object for different city
        // using supplied offset
        let nd = new Date(utc + (3600000*(offset -7)));
        
        // return time as a string
        return nd.toISOString().slice(0, 19).replace('T', ' ');

    }
    startInspection(id, companyName, routeName, pointId, pointName, pointType, pointSubType, pointSubType2, pointSubType3, pointIdTour, inspPointName,inspPointHidrostatica, barcode,item_barcode, item_id){
      this.loader = this.loadingCtrl.create({
          content: "Iniciando Inspeccion...",
          //duration: 10000
      });
      this.loader.present();

      let lat: any = "";
      let lng: any = "";
      let startDate = this.calcTime("", -5);
      this.geolocation.getCurrentPosition().then((resp) => {

        lat = resp.coords.latitude;
        lng = resp.coords.longitude;
        


        console.log(startDate);
        this.inspectionsService.update(id,{'status':1, 'started_at': startDate, 'started_lat':lat, 'started_lng':lng})
          .subscribe(
            (response) => {

              this.loader.dismiss();
              //this.presentToast("Inspección finalizada exitosamente");
              this.inspectionStts = 'iniciada';
              if (pointIdTour == null) {
                this.navCtrl.push(InspectionToursPage, {inspectionId:id,companyName:companyName,routeName:routeName,pointId:pointId,pointName:pointName,pointType:pointType,pointSubType:pointSubType,pointSubType2:pointSubType2,pointSubType3:pointSubType3,pointIdTour:pointIdTour,inspPointName:inspPointName, hidrostatica:inspPointHidrostatica, item_id: item_id})

              }else{
                this.navCtrl.push(EditInspectionTourPage, {inspectionId:id,companyName:companyName,routeName:routeName,pointId:pointId,pointName:pointName,pointType:pointType,pointSubType:pointSubType,pointSubType2:pointSubType2,pointSubType3:pointSubType3,pointIdTour:pointIdTour,inspPointName:inspPointName, hidrostatica:inspPointHidrostatica, item_id: item_id})

              }
             // this.showInspectionTour(id, companyName, routeName, pointId, pointName, pointType, pointSubType, pointSubType2, pointSubType3, pointIdTour,inspPointName,inspPointHidrostatica, barcode, item_barcode, item_id);
            }
        );

      }).catch((error) => {
        console.log('Error getting location', error);
      });

      


    }
    presentPromptReagendar() {
      let alert = this.alertCtrl.create({
        title: 'Reagendar',
        inputs: [
          
          {
            name: 'Fecha',
            placeholder: 'Fecha',
            type: 'datetime-local'
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Guardar',
            handler: data => {
              console.log('Reagendar - ' + data);
              this.reagendarInspection(data.Fecha.replace('T'," "));
            }
          }
        ]
      });
      alert.present();
    }
    reagendarInspection(date){
      this.loader = this.loadingCtrl.create({
          content: "Reagendando Inspeccion...",
          //duration: 10000
      });
      this.inspectionsService.update(this.inspectionId,{'scheduled_at':date})
          .subscribe(
            (response) => {

              this.loader.dismiss();
              let toast = this.presentToast("Inspección reagendada");
              
              toast.onDidDismiss(() => {
                this.navCtrl.setRoot(HomePage);
              });
              
            }
        );
    }
    startConfirm(id, companyName, routeName, pointId, pointName, pointType, pointSubType, pointSubType2, pointSubType3, pointIdTour, inspPointName,inspPointHidrostatica, barcode,item_barcode, item_id) {
      let alert = this.alertCtrl.create({
        title: 'Seleccion acción deseada',
        message: "",
        buttons: [
          {
            text: 'Reagendar',
            role: 'Reagendar',
            handler: () => {
              //console.log('Cancel clicked');
              this.presentPromptReagendar();
            }
          },
          {
            text: 'Iniciar',
            handler: () => {
              //console.log('Buy clicked');
              this.startInspection(id, companyName, routeName, pointId, pointName, pointType, pointSubType, pointSubType2, pointSubType3, pointIdTour, inspPointName,inspPointHidrostatica, barcode,item_barcode, item_id);
            }
          },
          {
            text: 'Cancelar',
            handler: () => {
              //console.log('Buy clicked');
              //this.startInspection(id, companyName, routeName, pointId, pointName, pointType, pointSubType, pointSubType2, pointSubType3, pointIdTour, inspPointName,inspPointHidrostatica);
            }
          }
        ]
      });
      alert.present();
    }

    presentToast(msg) {
      let toast = this.toastCtrl.create({
        message: msg,
        duration: 2000,
        position: 'bottom',
        dismissOnPageChange: true
      });

      

      toast.present();
      return toast;
    }

    gotoAddInspectionPoint(routeId: string,companyName: string,routeName: string){
    //this.navCtrl.setRoot(AddInspectionPointPage);
      this.navCtrl.push(AddInspectionPointPage, {inspectionId:this.inspectionId, routeId: routeId,companyName: companyName,routeName: routeName})
    }
    gotoAddRouteZone(routeId: string,companyName: string,routeName: string){
    //this.navCtrl.setRoot(AddInspectionPointPage);
      this.navCtrl.push(AddRouteZonePage, {inspectionId:this.inspectionId, routeId: routeId,companyName: companyName,routeName: routeName})
    }

    showInspectionTour(inspectionId: string,companyName: string,routeName: string,pointId: string, pointName: string, pointType: string, pointSubType: string, pointSubType2: string, pointSubType3: string, pointIdTour: string, inspPointName: string, inspPointHidrostatica: string, barcode: string, item_barcode: string, item_id: string){
      

        this.showModalBarcodes(inspectionId, companyName, routeName, pointId, pointName, pointType, pointSubType, pointSubType2, pointSubType3, pointIdTour, inspPointName,inspPointHidrostatica, barcode,item_barcode, item_id);
      
      
    }
    showModalBarcodes(inspectionId, companyName, routeName, pointId, pointName, pointType, pointSubType, pointSubType2, pointSubType3, pointIdTour, inspPointName,inspPointHidrostatica, barcode,item_barcode, item_id){
      let modal: Modal = this.modalCtrl.create("ScanbarcodesPage", {inspectionId:inspectionId, barcode: barcode, item_barcode:item_barcode,pointId:pointId, pointType: pointType});
      
      modal.onDidDismiss(data => {
        console.log(data);
        
        //console.log(this.barcode); 
        if (data) {

          if ( this.inspectionStts == 'agendada') {
            this.startConfirm(inspectionId, companyName, routeName, pointId, pointName, pointType, pointSubType, pointSubType2, pointSubType3, pointIdTour, inspPointName,inspPointHidrostatica, barcode,item_barcode, item_id);
            
          }else{


            if (pointIdTour == null) {
              this.navCtrl.push(InspectionToursPage, {inspectionId:inspectionId,companyName:companyName,routeName:routeName,pointId:pointId,pointName:pointName,pointType:pointType,pointSubType:pointSubType,pointSubType2:pointSubType2,pointSubType3:pointSubType3,pointIdTour:pointIdTour,inspPointName:inspPointName, hidrostatica:inspPointHidrostatica, item_id: item_id})

            }else{
              this.navCtrl.push(EditInspectionTourPage, {inspectionId:inspectionId,companyName:companyName,routeName:routeName,pointId:pointId,pointName:pointName,pointType:pointType,pointSubType:pointSubType,pointSubType2:pointSubType2,pointSubType3:pointSubType3,pointIdTour:pointIdTour,inspPointName:inspPointName, hidrostatica:inspPointHidrostatica, item_id: item_id})

            }
          }
          
        }
      });

      modal.present();
    }
    showModal(){

      let modal: Modal = this.modalCtrl.create("ModalPage");
      
      modal.onDidDismiss(data => {
        console.log(data);
        this.barcode = data;
        if (this.barcode in this.inspectionPointsBarcode) {

          
            let id = this.inspectionPointsBarcode[this.barcode].id.id;
            let companyName = this.inspectionPointsBarcode[this.barcode].companyName;
            let routeName = this.inspectionPointsBarcode[this.barcode].routeName;
            let pointid = this.inspectionPointsBarcode[this.barcode].pointid;
            let pointIdTour = this.inspectionPointsBarcode[this.barcode].pointIdTour;
            let pointname = this.inspectionPointsBarcode[this.barcode].pointname;
            let inspPointName = this.inspectionPointsBarcode[this.barcode].inspPointName;
            let pointinspection_point_type = this.inspectionPointsBarcode[this.barcode].pointinspection_point_type;
            let pointinspection_point_sub_type  = this.inspectionPointsBarcode[this.barcode].pointinspection_point_sub_type;
            let pointinspection_point_sub_type2  = this.inspectionPointsBarcode[this.barcode].pointinspection_point_sub_type2;
            let pointinspection_point_sub_type3  = this.inspectionPointsBarcode[this.barcode].pointinspection_point_sub_type3;
            let pointinspection_point_idTour  = this.inspectionPointsBarcode[this.barcode].pointinspection_point_sub_type;

            let inspPointHidrostatica = this.inspectionPointsBarcode[this.barcode].inspPointHidrostatica;
            this.item_barcode = this.inspectionPointsBarcode[this.barcode].item_barcode;

            if (pointinspection_point_type !== 'extintor' && pointinspection_point_type !== 'camilla') {

              if ( this.inspectionStts == 'agendada') {
                  this.startConfirm(id, companyName, routeName, pointid, pointname, pointinspection_point_type, pointinspection_point_sub_type, pointinspection_point_sub_type2, pointinspection_point_sub_type3, pointIdTour, inspPointName,inspPointHidrostatica, this.barcode,this.item_barcode, this.item_id);
                  
                }else{


                  if (pointIdTour == null) {
                    this.navCtrl.push(InspectionToursPage, {inspectionId:id,companyName:companyName,routeName:routeName,pointId:pointid,pointName:pointname,pointType:pointinspection_point_type,pointSubType:pointinspection_point_sub_type,pointSubType2:pointinspection_point_sub_type2,pointSubType3:pointinspection_point_sub_type3,pointIdTour:pointIdTour,inspPointName:inspPointName, hidrostatica:inspPointHidrostatica, item_id: this.item_id})

                  }else{
                    this.navCtrl.push(EditInspectionTourPage, {inspectionId:id,companyName:companyName,routeName:routeName,pointId:pointid,pointName:pointname,pointType:pointinspection_point_type,pointSubType:pointinspection_point_sub_type,pointSubType2:pointinspection_point_sub_type2,pointSubType3:pointinspection_point_sub_type3,pointIdTour:pointIdTour,inspPointName:inspPointName, hidrostatica:inspPointHidrostatica, item_id: this.item_id})

                  }
                }

            }else{
              this.showInspectionTour(id,companyName,routeName,pointid,pointname,pointinspection_point_type,pointinspection_point_sub_type,pointinspection_point_sub_type2,pointinspection_point_sub_type3,pointIdTour,inspPointName,inspPointHidrostatica, this.barcode,this.item_barcode, this.item_id);
            }
          }else{
            this.presentToast("Punto de inspección NO registrado");
          }
      });

      modal.present();
    }

    showModalSetting(){

      let modal: Modal = this.modalCtrl.create("SettingsPage",{ started_at:  this.startDate});
      
      modal.onDidDismiss(data => {
        if (data != null && data != undefined) {
          this.loader = this.loadingCtrl.create({
            content: "Actualizando Inspeccion...",
            //duration: 10000
          });
          this.loader.present();
          console.log(data);
          this.startDate = data;
          this.inspectionsService.update(this.inspectionId,{ 'started_at': this.startDate})
            .subscribe(
              (response) => {

                this.loader.dismiss();
                let toast = this.presentToast("Inspección actualizada exitosamente");
                
                
              }
          );
        }
      });

      modal.present();
    }

    gotoHome(){
      this.navCtrl.push(HomePage, {refresher: true});
    }
    getReporteApp(){
      this.storage.get('authData')
        .then((data) => {
          let authData = JSON.parse(data);
          if (authData != null ) {
            let access_token = authData.access_token;
            let client = authData.client;
            let uid = authData.uid;
            
            
            window.open(AppSettings.APP_WEBSITE+"reportes/reportepdf.php?id="+this.inspectionId+"&token="+access_token+"&client="+client+"&uid="+uid+"",'_system', 'location=yes');
  
          }         
        }
      )
    }
}
