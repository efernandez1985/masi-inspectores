import { Component } from '@angular/core';
import { NavController, App, LoadingController, ToastController,AlertController, NavParams } from 'ionic-angular';
import { AuthService } from '../../services/auth';
import { LoginPage } from '../login/login';
import { InspectionsService } from "../../services/inspections";
import { RouteZonesService } from "../../services/route-zones";
import { InspectionPage } from "../inspections/show";
import { Geolocation } from '@ionic-native/geolocation';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import {ProgressBarModule} from "angular-progress-bar"
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  protected inspections: any[] = [];
  loader: any;
  saveExit = false;
  totalPorcent: number = 0;
  refresher: any = false;
  userId = '';
  constructor(public app: App,
      private navParams: NavParams, public navCtrl: NavController, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController,
              protected inspectionsService: InspectionsService,
      protected routeZonesService: RouteZonesService,
      private alertCtrl: AlertController,
      private geolocation: Geolocation,
      private androidPermissions: AndroidPermissions,
      private locationAccuracy: LocationAccuracy,
      public storage: Storage) {}

  ngOnInit(): void {

    this.saveExit = this.navParams.get('saveExit');
    this.refresher = this.navParams.get('refresher');
    this.getInspections(this.refresher);

    this.requestPermission();
  }
  doRefresh(refresher) {
    
    this.refresher = refresher;
    this.getInspections(true);
    
    
  }

  protected getInspections(refresherFlag = false) {
    this.inspections = [];
    this.loader = this.loadingCtrl.create({
      content: "Cargando Inspecciones...",
      duration: 10000
    });
    this.loader.present();

    this.storage.get('authData')
      .then((data) => {
        let authData = JSON.parse(data);
        if (authData != null && authData.userId != undefined) {
          this.userId = authData.userId;
          this.inspectionsService.getbyuser(this.userId)
      .subscribe(
        (response) => {
          this.inspections = JSON.parse(response.text());
          console.log(this.inspections);
          this.inspections.forEach( val=> {

            this.routeZonesService.showjointours(val.route_id,val.id)
              .subscribe(
                (response) => {
                  //console.log(response);
                  let ip = JSON.parse(response.text());
                  //console.log(insp);
                  //let ip = JSON.parse(responsePoint.text())['inspection_points'];
                  console.log(ip);
                  let totalPoint = 0;
                  let pointTours = 0;
                  this.totalPorcent = 0;
                  ip.forEach( val2=> {

                    
                    if (val2.inspection_point_type != null) {
                      totalPoint++;
                      if (val2.statusTour == 1) {
                        pointTours++;
                      }

                    }

                  });
                  this.totalPorcent = (totalPoint > 0 ) ? (100*pointTours)/totalPoint : 0;
                  //this.totalPorcent = parseFloat(this.totalPorcent).toFixed(2);
                  this.totalPorcent = parseFloat(this.totalPorcent.toFixed(2));
                  //val.totalPorcent = this.totalPorcent;
                  val.totalPorcent = this.totalPorcent;
                }
            );

            if ( !this.saveExit && val.status == 'iniciada' && !refresherFlag ) {
              this.navCtrl.push(InspectionPage, {inspectionId: val.id});
              return false;
            }

          });
          this.loader.dismiss();
          if (refresherFlag) {
            this.refresher.complete();
          }
        }
      );
         
        }
    })
    
  }

  protected showInspection(inspectionId: string) {
    this.navCtrl.push(InspectionPage, {inspectionId: inspectionId})
  }
  startInspection(id){
    this.loader = this.loadingCtrl.create({
        content: "Iniciando Inspeccion...",
        //duration: 10000
    });
    this.loader.present();

    let lat: any = "";
    let lng: any = "";
    let startDate = this.calcTime("", -5);
    this.geolocation.getCurrentPosition().then((resp) => {

      lat = resp.coords.latitude;
      lng = resp.coords.longitude;
      


      console.log(startDate);
      this.inspectionsService.update(id,{'status':1, 'started_at': startDate, 'started_lat':lat, 'started_lng':lng})
        .subscribe(
          (response) => {

            this.loader.dismiss();
            //this.presentToast("Inspección finalizada exitosamente");
            this.navCtrl.push(InspectionPage, {inspectionId: id})
          }
      );

    }).catch((error) => {
      console.log('Error getting location', error);
    });

    


  }
  requestPermission(){
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {

      if(canRequest) {
        // the accuracy option will be ignored by iOS
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          () => console.log('Request successful'),
          error => console.log('Error requesting location permissions', error)
        );
      }
    });
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(
          result => {
            console.log('Has permission?',result);
            if(!result.hasPermission){
              this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(
                result => {
                  console.log('Has permission?', result);
                  if(!result.hasPermission){
                    //this.navCtrl.popToRoot()
                  }
              },
                err => console.log('Error requestPermission: ', err)
            );
            }
          },
          err => console.log('Error checkPermission: ', err)
      );
  }
  startConfirm(id,company,route) {
      let alert = this.alertCtrl.create({
        title: 'Iniciar inspección',
        message: company+'<br>' + route+'<br>'+'¿Desea iniciar esta inspección?',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              //console.log('Cancel clicked');
            }
          },
          {
            text: 'Iniciar',
            handler: () => {
              //console.log('Buy clicked');
              this.startInspection(id);
            }
          }
        ]
      });
      alert.present();
    }

    calcTime(city, offset) {

        // create Date object for current location
        let d = new Date();
        
        // convert to msec
        // add local time zone offset 
        // get UTC time in msec
        let utc = d.getTime() + (d.getTimezoneOffset() * 60000);
        
        // create new Date object for different city
        // using supplied offset
        let nd = new Date(utc + (3600000*(offset -7)));
        
        // return time as a string
        return nd.toISOString().slice(0, 19).replace('T', ' ');

    }
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      // console.log('Dismissed toast');
    });

    toast.present();
  }

}
