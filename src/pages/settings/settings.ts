import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
	started_at = '';
  constructor(public navCtrl: NavController, public navParams: NavParams, private view: ViewController) {
  }
  	ngOnInit(): void {
  		this.started_at = this.navParams.get('started_at');
	}
  closeModal(){

  	this.view.dismiss();
  }
  sendStaredAt(){
  	console.log(this.started_at);
  	this.view.dismiss(this.started_at);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

}
