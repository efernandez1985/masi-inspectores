import { Component } from '@angular/core';
import { NavController, App, LoadingController, NavParams, Platform,Modal, ModalController, ToastController  } from 'ionic-angular';
import { AuthService } from '../../services/auth';
import { CompaniesService } from "../../services/companies";
import { RoutesService } from "../../services/routes";
import { InspectionsService } from "../../services/inspections";
import { Storage } from '@ionic/storage';
import { AppSettings } from "../../app/app.settings";
/**
 * Generated class for the ReportsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reports',
  templateUrl: 'reports.html',
})
export class ReportsPage {
	inspection = '';
	userId = '';
	routeName = "";
	companies = [];
  	routes = [];
  	constructor(
	    public navCtrl: NavController,
	    public navParams: NavParams,
	    public loadingCtrl: LoadingController,
	    public companiesService: CompaniesService,
	    public routesService: RoutesService,
	    public modalCtrl: ModalController,
	    private toastCtrl: ToastController,
      	protected inspectionsService: InspectionsService,
      	public storage: Storage

	) {}

  	ngOnInit(): void {


      // this.inspectionPoints = this.navParams.get('inspectionPoints');
      // this.companyName = this.navParams.get('companyName');
      // this.routeName = this.navParams.get('routeName');
      this.getCompanies();
  	}
	getCompanies(){
	    let loader = this.loadingCtrl.create({
	      content: "Cargando datos...",
	      duration: 10000
	    });
	    loader.present();

	    this.companiesService.index()
	      .subscribe(
	        (response) => {
	          let c = JSON.parse(response.text());
	          console.log(c);
	          this.companies = c;
	          //this.companyName = insp.company.name;
	          //this.routeName = insp.route.name;
	          loader.dismiss();
	          this.routes = [];
	          
	        }
	      );


	}
	getRoute(idCompany){
	    console.log('idCompany = '+ idCompany);
	    this.routesService.getbycompany(idCompany)
	      .subscribe(
	        (response) => {
	          let r = JSON.parse(response.text());
	          console.log(r);
	          this.routes = [];
	          
	          r.forEach( val=> {
	            let route = val.route
	            let rz = val.route.route_zone
	            this.routes.push(route);
	            //this.route_zone
	          });

	          //this.routes = r;
	          //this.companyName = insp.company.name;
	          //this.routeName = insp.route.name;
	          //loader.dismiss();
	        }
	      );
	}
	getLastInspection(routeId){
		this.storage.get('authData')
      	.then((data) => {
	        let authData = JSON.parse(data);
	        if (authData != null && authData.userId != undefined) {
		        this.userId = authData.userId;
		        
			    this.inspectionsService.getlastinspection(routeId,this.userId)
		        .subscribe(
		            (response) => {
		              //console.log(response);
		              let insp = JSON.parse(response.text());
		              console.log(insp);
		              this.inspection = insp;
		              
		            }
		        );
	         
	        }
    	})
		

	}
	getReporteApp(inspId){
      this.storage.get('authData')
        .then((data) => {
          let authData = JSON.parse(data);
          if (authData != null ) {
            let access_token = authData.access_token;
            let client = authData.client;
            let uid = authData.uid;
            
            
            window.open(AppSettings.APP_WEBSITE+"reportes/reportepdf.php?id="+inspId+"&token="+access_token+"&client="+client+"&uid="+uid+"",'_system', 'location=yes');
  
          }         
        }
      )
    }
	presentToast(msg) {
	      let toast = this.toastCtrl.create({
	        message: msg,
	        duration: 2000,
	        position: 'bottom',
	        dismissOnPageChange: true
	      });

	      

	      toast.present();
	      return toast;
	}
}
