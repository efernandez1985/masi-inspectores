import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddInspectionPointPage } from './add-inspection-point';

@NgModule({
  declarations: [
    AddInspectionPointPage,
  ],
  imports: [
    IonicPageModule.forChild(AddInspectionPointPage),
  ],
})
export class AddInspectionPointPageModule {}
