import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NavController, App, LoadingController, ToastController, NavParams } from 'ionic-angular';
import { AuthService } from '../../services/auth';
import { InspectionsService } from "../../services/inspections";
import { InspectionPointsService } from "../../services/inspection-points";
import { RoutesService } from "../../services/routes";
import { InspectionPage } from "../inspections/show";
import { ZBar, ZBarOptions } from '@ionic-native/zbar';

/**
 * Generated class for the AddInspectionPointPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()



@Component({
  selector: 'page-add-inspection-point',
  templateUrl: 'add-inspection-point.html',
})

export class AddInspectionPointPage {
  routeId: string;
  inspectionId: string;
	companyName: string;
	routeName: string;
	protected routeZones = [];
  pointType = '';
  pointSubType = '';
  pointSubType2 = '';
  pointSubType3 = '';
  sub_type_arr = [];
  sub_type_arr2 = [];
  sub_type_arr3 = [];
  sub_type = {
    "extintor" : {
      "PORTATIL" : {
        "PQS ABC":[
          "0,75 KG",
          "1,00 KG",
          "2,0 KG",
          "4,5 KG",
          "6,0 KG",
          "9,0 KG",
          "13,0 KG"

        ],
        "PQS BC BICARBONATO DE SODIO":[
          "0,75 KG",
          "1,00 KG",
          "2,0 KG",
          "4,5 KG",
          "6,0 KG",
          "9,0 KG"

        ],
        "CLASE D":[
          "13,0 KG",
          "15,0 KG"

        ],
        "WATER MIST":[
          "6,0 LT",
          "9,0 LT"

        ],
        "CO2":[
          "2,2 KG",
          "4,5 KG",
          "6,8 KG",
          "9,0 KG"

        ],
        "AGUA":[
          "6,0 LT",
          "9,0 LT"

        ],
        "CLASE K":[
          "6,0 LT",
          "9,0 LT"

        ],
        "AFFF":[
          "6,0 LT",
          "9,0 LT"
        ],
        "MICROBLAZE":[
          "6,0 LT",
          "9,0 LT"
        ],
        "HALOTRON":[
          "0,75 KG",
          "1,0 KG",
          "2,2 KG",
          "2,5 KG",
          "5,0 KG",
          "7,0 KG"

        ],
        "FE-36":[
          "0,75 KG",
          "1,00 KG",
          "2,0 KG",
          "4,5 KG",
          "6,0 KG",
          "9,0 KG"

        ],
        "PURPURA K":[
          "1,0 KG",
          "2,2 KG",
          "4,5 KG",
          "6,0 KG",
          "9,0 KG"

        ]

      },
      "UNIDAD MOVIL" :{
        "PQS ABC":[
          "34,0 KG",
          "50,0 KG",
          "68,0 KG",
          "100,0 KG"

        ],
        "CO2":[
          "22,5 KG",
          "45,0 KG"

        ],
        "AFFF":[
          "50,0 LT"

        ],
        "MICROBLAZE":[
          "50,0 LT"

        ]


      }

    },
    "hidrante" : [
      "HIDRANTE 1 1/2\" x 15 mts",
      "HIDRANTE 1 1/2\" x 30 mts",
      "HIDRANTE 2 1/2\" X 15 mts",
      "HIDRANTE 2 1/2\" X 30 mts",
      "HIDRANTE-ESPUMA 1 1/2\" x 30 mts"

    ],
    "regadera" : [
      "LAVAOJOS FIJO",
      "LAVAOJOS PORTATIL",
      "REGADERA ACERO GALVANIZADO",
      "REGADERA DE ACERO INOXIDABLE",
      "REGADERA DE RECUBRIMIENTO PLASTICO",
      "REGADERA C/LAVAOJOS DE ACERO GALVANIZADO",
      "REGADERA C/LAVAOJOS DE ACERO INOXIDABLE",
      "REGADERA C/LAVAOJOS DE ACERO CON RECUBRIMIENTO PLASTICO"

    ],
    "camilla" : [
      "CAMILLA RIGIDA",
      "CAMILLA TIPO CANASTILLA PLASTICA",
      "CAMILLA TIPO CANASTILLA DE REJILLA",
      "CAMILLA TIPO MILITAR",
      "CAMILLA TIPO SKED"

      

    ],
    "bombero" : [
      "EQUIPO EN PLANTA"
    ]
    
  };
  lbl_subType: string = '';
  lbl_subType2: string = '';
  lbl_subType3: string = '';
  barcode = "";
  item_barcode = "";
  ubicacion = "";
   	constructor(private navParams: NavParams,
        public navCtrl: NavController,
        public authService: AuthService,
        public loadingCtrl: LoadingController,
        private toastCtrl: ToastController,
        protected inspectionsService: InspectionsService,
        protected inspectionPointsService: InspectionPointsService,
        protected routesService: RoutesService,
        private zbar: ZBar) {}
  

	ngOnInit(): void {
		this.getRouteZones();
	}
  scanBarcode(e,type){
    e.preventDefault();
    this.zbar.scan({flash: 'on'}).then(barcodeData => {
      console.log('Barcode data', barcodeData);
      if (type ==1) {
        this.barcode = barcodeData;
      }else{
        this.item_barcode = barcodeData;
      }
    }).catch(err => {
      console.log('Error', err);
    });
  }
	protected getRouteZones() {
    this.routeZones = [];
    this.routeId = this.navParams.get('routeId');
    this.companyName = this.navParams.get('companyName');
    this.routeName = this.navParams.get('routeName');
    this.inspectionId = this.navParams.get('inspectionId');
    let loader = this.loadingCtrl.create({
      content: "Cargando datos...",
      duration: 10000
    });
    loader.present();

    this.routesService.show(this.routeId)
      .subscribe(
        (response) => {
          console.log(response);
          let rz = JSON.parse(response.text());
          this.routeZones = rz.route_zones;
          //this.companyName = insp.company.name;
          //this.routeName = insp.route.name;
          loader.dismiss();
        }
      );
  }

  saveInspectionPoint(form: NgForm){
      let inspectionPointData = {
        "name": form.value.name,
        "route_zone_id": form.value.routeZone, 
        "inspection_point_type": form.value.pointType, 
        "inspection_point_sub_type": form.value.pointSubType,
        "inspection_point_sub_type2": form.value.pointSubType2,
        "inspection_point_sub_type3": form.value.pointSubType3,
        "barcode": form.value.barcode,
        "hidrostatica": form.value.hidrostatica,
        "location": form.value.ubicacion,
        "item_name": form.value.item_name,
        "item_barcode": form.value.item_barcode,
      };
      let loader = this.loadingCtrl.create({
          content: "Guardando...",
          duration: 10000
      });
      loader.present();
      this.inspectionPointsService.create(inspectionPointData)
      .subscribe(
        (response) =>{
          console.log(response);
          loader.dismiss();
          let toast = this.presentToast("Guardado correctamente");
          toast.onDidDismiss(() => {
            this.navCtrl.push(InspectionPage, {inspectionId: this.inspectionId})
          });
        },
        (err) => {
          console.log(err)
          loader.dismiss();
          this.presentToast("Error: Código de barras en uso");
        }


      );
  }
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    

    toast.present();
    return toast;
  }

  onTypeChange(selectedValue: any){

    //alert(selectedValue);
    this.lbl_subType = selectedValue;
    console.log(this.sub_type[selectedValue]);
    //this.sub_type_arr = this.sub_type[selectedValue];
    this.sub_type_arr = [];
    if (selectedValue == 'extintor') {
      for (let key in this.sub_type[selectedValue]) {
        let value = this.sub_type[selectedValue][key];
        console.log(key);

        this.sub_type_arr.push(key);
      }
    }else{
      this.sub_type_arr = this.sub_type[selectedValue];
    }
       

  }
  onTypeChange2(selectedValue: any){

    //alert(selectedValue);
    this.lbl_subType2 = selectedValue;
    //console.log(this.sub_type_arr[selectedValue]);
    //this.sub_type_arr = this.sub_type[selectedValue];
    this.sub_type_arr2 = [];
    
    for (let key in this.sub_type[this.pointType][selectedValue]) {
      let value = this.sub_type[this.pointType][selectedValue][key];
      console.log(key);
      this.sub_type_arr2.push(key);
    }
       

  }
  onTypeChange3(selectedValue: any){

    //alert(selectedValue);
    this.lbl_subType3 = selectedValue;
    //console.log(this.sub_type_arr[selectedValue]);
    //this.sub_type_arr = this.sub_type[selectedValue];

    this.sub_type_arr3 = [];
    for (let key in this.sub_type[this.pointType][this.pointSubType][selectedValue]) {
      let value = this.sub_type[this.pointType][this.pointSubType][selectedValue][key];
      console.log(key);
      this.sub_type_arr3.push(value);
    }
       

  }


}
