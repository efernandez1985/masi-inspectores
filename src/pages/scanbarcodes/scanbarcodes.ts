import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, ToastController, NavParams, AlertController, ViewController } from 'ionic-angular';

import { InspectionToursService } from "../../services/inspection-tours";
import { InspectionPage } from "../inspections/show";
import { HomePage } from '../home/home';

import { ZBar, ZBarOptions } from '@ionic-native/zbar';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AndroidPermissions } from '@ionic-native/android-permissions';
/**
 * Generated class for the ScanbarcodesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-scanbarcodes',
  templateUrl: 'scanbarcodes.html',
})
export class ScanbarcodesPage {
  inspectionId = '';
  pointId = '';
	scanItemDiv = false;
	barcode = '';
  param_barcode = '';
  param_item_barcode = '';
	item_barcode = '';
	inspectionPointsBarcode = {};
  pointType = '';
  public photosAccess : any;
  public base64Image : string;
	constructor(public navCtrl: NavController, public navParams: NavParams, private view: ViewController,
      private zbar: ZBar,
      private toastCtrl: ToastController,
      private alertCtrl: AlertController,  
      private camera: Camera,
      protected inspectionToursService: InspectionToursService,
      private androidPermissions: AndroidPermissions,
      public loadingCtrl: LoadingController) {
	}
	ngOnInit(): void {
      this.inspectionId = this.navParams.get('inspectionId');
      this.pointId = this.navParams.get('pointId');
      this.param_barcode = this.navParams.get('barcode');
      this.param_item_barcode = this.navParams.get('item_barcode');
      this.pointType = this.navParams.get('pointType');
      // console.log('inspectionId = '+ this.inspectionId);
      // console.log('pointId = '+ this.pointId);
      // console.log('param_barcode = '+ this.param_barcode);
      // console.log('param_item_barcode = '+ this.param_item_barcode);
      // console.log('pointType = '+ this.pointType);
      this.requestPermission();
	}
  requestPermission(){
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
          result => {
            console.log('Has permission?',result);
            if(!result.hasPermission){
              this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA).then(
                result => {
                  console.log('Has permission?', result);
                  if(!result.hasPermission){
                    //this.navCtrl.popToRoot()
                  }
              },
                err => console.log('Error requestPermission: ', err)
            );
            }
          },
          err => console.log('Error checkPermission: ', err)
      );
  }
	presentToast(msg) {
      let toast = this.toastCtrl.create({
        message: msg,
        duration: 2000,
        position: 'bottom',
        dismissOnPageChange: true
      });

      

      toast.present();
      return toast;
    }
	closeModal(){

	  	this.view.dismiss(false);
	}
	getByBarcode(type){
        
        this.zbar.scan({flash: 'on'}).then(barcodeData => {
          console.log('Barcode data', barcodeData);

          //inspection point
          if (type == 1) {
            if (this.param_barcode == barcodeData) {

              if (this.pointType == 'extintor' || this.pointType == 'camilla') {

                this.scanItemDiv = true;
              	this.barcode = this.param_barcode;
              }else{
                this.item_barcode = this.param_item_barcode;
                this.view.dismiss(true);
              }

            }else{

              this.presentToast("Punto de inspección NO registrado");
              this.scanItemDiv = false;

            }

          }else{//item
            if (this.param_item_barcode == barcodeData) {

            
              //this.showInspectionTour(id,companyName,routeName,pointid,pointname,pointinspection_point_type,pointinspection_point_sub_type,pointinspection_point_sub_type2,pointinspection_point_sub_type3,pointIdTour,inspPointName,inspPointHidrostatica);
              //this.scanItemDiv = true;
              this.item_barcode = this.param_item_barcode;
              this.view.dismiss(true);
            }else{
              
              this.presentPromptError();
              //this.scanItemDiv = false;
            }
          }
            

        }).catch(err => {
          console.log('Error', err);
        });
          
          
  }
  searchBarcode(type){

    if (type == 1) {
      if (this.barcode == this.param_barcode) {

        this.scanItemDiv = true;

      }else{
        this.presentToast("Punto de inspección NO registrado");
        this.scanItemDiv = false;
      }
    }else{
      if (this.item_barcode == this.param_item_barcode) {

        this.view.dismiss(true);
        //this.presentToast("OK");
      }else{
        
        //this.presentToast("Equipo NO registrado en este punto de inspección");
        //this.scanItemDiv = false;
        this.presentPromptError();
      }

    }

  }
  presentPromptError() {
    let alert = this.alertCtrl.create({
      title: 'Equipo no encontrado',
      message: 'El código del equipo asignado a este punto de inspección es: '+this.param_item_barcode+' ¿Desea contianuar?',
      buttons: [
        {
          text: 'NO',
          role: 'cancel',
          handler: data => {
            this.view.dismiss(false);
          }
        },
        {
          text: 'SI',
          handler: data => {
            this.view.dismiss(true);
          }
        }
      ]
    });
    alert.present();
  }

  takePhotoAccess(e) {
    e.preventDefault();
    const options : CameraOptions = {
      quality: 50, // picture quality
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetHeight: 512,
      targetWidth : 512
    }
    this.camera.getPicture(options) .then((imageData) => {
        this.base64Image = "data:image/jpeg;base64," + imageData;
        this.photosAccess = this.base64Image;
        
        this.saveInspectionTour(2);
      }, (err) => {
        console.log(err);
    });
        
  }

  public saveInspectionTour(status = 1){
    let loader = this.loadingCtrl.create({
            content: "Guardando...",
            duration: 10000
      });

      loader.present();
    let inspectionTourData = {
      "inspection_id": this.inspectionId,
      "inspection_point_id": this.pointId,
      "status": status,
      
      "photoAccess": (this.photosAccess) ? this.photosAccess : ""


    };

      
      this.inspectionToursService.create(inspectionTourData)
      .subscribe(
          (response) =>{
            let it = JSON.parse(response.text());
            console.log(it);
            loader.dismiss();
            let toast = this.presentToast("Guardado correctamente");
            toast.onDidDismiss(() => {
              //this.navCtrl.setRoot(HomePage);
              if(status == 0){
                //this.platform.exitApp();
                this.navCtrl.setRoot(HomePage, {saveExit: true});
              }else{
                this.navCtrl.push(InspectionPage, {inspectionId: this.inspectionId})
              }
            });
          }


      );

  }

}
