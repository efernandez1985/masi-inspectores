import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScanbarcodesPage } from './scanbarcodes';

@NgModule({
  declarations: [
    ScanbarcodesPage,
  ],
  imports: [
    IonicPageModule.forChild(ScanbarcodesPage),
  ],
})
export class ScanbarcodesPageModule {}
